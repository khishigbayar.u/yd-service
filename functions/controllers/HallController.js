const express = require("express");
const router = express.Router();

const admin = require("firebase-admin");
admin.initializeApp();

router.get("/getHalls", async (req, res) => {
    const hallCollection = await admin.firestore().collection('HALLS').get();
    console.log(hallCollection.docs.map(hallDoc => hallDoc.data()));
    res.status(200).json(hallCollection.docs.map(hallDoc => hallDoc.data()));
});

module.exports = router;