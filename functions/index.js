const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");

require("dotenv").config();
const app = express();
app.use(cors());
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

// Firebase
// const admin = require("firebase-admin");
// admin.initializeApp();

// Router
const hallController = require("./controllers/HallController");
app.use("/hall", hallController);

app.get("/", function (req, res) {
    res.send("Hello World");
});

exports.api = functions.https.onRequest(app);
